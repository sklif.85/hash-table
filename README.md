Hash table lookup strings

 *  Hash table is an array of hash_entries; each entry is a pointer to a string and a user-supplied value 1 char* wide.

 *  The array always has 2 ** n elements, n>0, n integer.
 *  When the array gets too full, we create a new array twice as large and re-hash the symbols into the new array, then forget the old array.
 *  Copy the values into the new array before we junk the old array.
 

Programing language -> C