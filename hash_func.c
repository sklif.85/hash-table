//
// Created by alex on 15.02.18.
//

#include "hash-table.h"

#define MULTIPLIER (37)

unsigned long hash_func(const char *s) {

    printf("hash_func");

    unsigned long h;
    unsigned const char *us;

    /* cast s to unsigned const char * */
    /* this ensures that elements of s will be treated as having values >= 0 */
    us = (unsigned const char *) s;

    h = 0;
    while (*us != '\0') {
        h = h * MULTIPLIER + *us;
        us++;
    }
    return h;
}
