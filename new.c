//
// Created by alex on 15.02.18.
//


#include "hash-table.h"

void *new(t_hash_table **hash_table, int n) {
    t_hash_table *new_table;

    if ((new_table = (t_hash_table *)malloc(sizeof(t_hash_table) * 1)) == NULL)
        handle_errors("malloc hash_table");

    if ((new_table->array = (t_hash_array *)malloc(sizeof(t_hash_array) * n)) == NULL)
        handle_errors("malloc new_table->array");

    new_table->count = (*hash_table)->count;
    new_table->size = (*hash_table)->size;

    *hash_table = new_table;
}

