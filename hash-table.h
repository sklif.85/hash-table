//
// Created by alex on 15.02.18.
//

#ifndef HASH_TABLE_HASH_TABLE_H
#define HASH_TABLE_HASH_TABLE_H

#include <stdio.h>
#include <stdlib.h>


typedef struct  s_hash_array
{
    char        *key;
    char        *val;
}               t_hash_array;

typedef struct  s_hash_table
{
    int size;                       /* size of the pointer table */
    int count;                      /* number of elements stored */
    struct s_hash_array  *array;
}               t_hash_table;

typedef struct          s_hash
{
    struct s_hash_table  *table;
    void *(*New)(struct s_hash_table **hash_table, int n);
    void (*Get)(struct s_hash *hash, char *key);
    void (*Add)(struct s_hash *hash, char *key, char *value);
    void (*Remove)(struct s_hash *hash, char *key);
    void (*Keys)(struct s_hash *hash);
    void (*Items)(struct s_hash *hash);
    void (*Values)(struct s_hash *hash);
    void (*Get_len)(struct s_hash *hash);
    unsigned long (*Hash_func)(const char *value);
}                       t_hash;


int main(void);

/**
 * methods
 * */
void *new(t_hash_table **hash_table, int n);

void add_element(t_hash *hash, char *key, char *value);

//void get_element(t_hash_table *hash_table, char *key);
//void remove_element(t_hash_table *hash_table, char *key);
//void get_values(t_hash_table *hash_table);
//void get_keys(t_hash_table *hash_table);
//void get_items(t_hash_table *hash_table);
//void get_len(t_hash_table *hash_table);
unsigned long hash_func(const char *value);

/**
 * sub functions
 */
void init(t_hash *hash);
void handle_errors(char *message);

#endif //HASH_TABLE_HASH_TABLE_H
