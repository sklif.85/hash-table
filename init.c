//
// Created by alex on 16.02.18.
//

#include "hash-table.h"

void init(t_hash *hash)
{
    (*hash).New = &new;
//    (*hash).get = get_element;
    (*hash).Add = add_element;
//    (*hash).remove = remove_element;
//    (*hash).get_keys = get_keys;
//    (*hash).get_items = get_items;
//    (*hash).get_values = get_values;
//    (*hash).len = get_len;
    (*hash).Hash_func = &hash_func;

    (*hash).table = (t_hash_table *)malloc(sizeof(t_hash_table) * 1);
    (*hash).table->array = NULL;
    (*hash).table->size = 1;
    (*hash).table->count = 2;

}
