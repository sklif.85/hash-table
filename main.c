//
// Created by alex on 15.02.18.
//

#include "hash-table.h"


int main(void) {
    t_hash hash;

    init(&hash);

    hash.New(&hash.table, 10);

    char *data[5][2] = {{{"key0"}, {"val0"}}, {{"key1"}, {"val1"}}, {{"key2"}, {"val2"}}, {{"key3"}, {"val3"}}, {{"key4"}, {"val4"}}};
    int i = -1;
    int j = 0;

    while (++i < 5)
    {
        printf("%s : %s\n", data[i][j], data[i][j+1]);
        hash.Add(&hash, data[i][j], data[i][j+1]);
    }



    return 0;
}

